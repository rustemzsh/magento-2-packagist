<?php

namespace Wizkunde\Packagist\Plugin;

class AddPackage
{
    protected $orderRepository;
    protected $packagistHelper;
    protected $productRepository;

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Wizkunde\Packagist\Helper\Packagist $packagistHelper
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Wizkunde\Packagist\Helper\Packagist $packagistHelper
    ) {
        $this->orderRepository = $orderRepository;
        $this->packagistHelper = $packagistHelper;
        $this->productRepository = $productRepository;
    }

    /**
     * @param $subject
     * @return mixed
     */
    public function afterRegister(\Magento\Sales\Model\Order\Invoice $subject)
    {
        // Order may not be loaded always
        if($subject->getOrderId() != null) {
            $order = $this->orderRepository->get($subject->getOrderId());

            foreach($order->getAllItems() as $item) {
                if($item->getProductType() == 'downloadable') {
                    $this->packagistHelper->addPackage($order, $this->productRepository->getById($item->getProductId()));
                }
            }
        }


        return $subject;
    }
}